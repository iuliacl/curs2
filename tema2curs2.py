operatii = ["+", "-", "*", "/"]
x = input("Introduceti primul numar: ")
a = float(x.replace(",","."))
op = input("Introduceti operatia dorita {} : ".format(operatii))

if op not in operatii:
    print("Operatia introdusa este gresita!")
    op = input("Introduceti operatia dorita {} : ".format(operatii))

y = input("Introduceti al doilea numar: ")
b = float(y.replace(",","."))

if op == operatii[0]:
    rez1 = a + b
    print("{} + {} =".format(f"{a:g}", f"{b:g}"), f"{rez1:g}")
elif op == operatii[1]:
    rez2 = a - b
    print("{} - {} =".format(f"{a:g}", f"{b:g}"), f"{rez1:g}")
elif op == operatii[2]:
    rez3 = a * b
    print("{} * {} =".format(f"{a:g}", f"{b:g}"), f"{rez1:g}")
elif op == operatii[3]:
    rez4 = a / b
    print("{} / {} =".format(f"{a:g}", f"{b:g}"), f"{rez1:g}")
else:
    print("Eroare!")
